<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use \kartik\datetime\DateTimePicker;
/* @var $this yii\web\View */


?>
<div class="site-index">
    <!--    Ip selection section-->
    <div class="row">
        <div class="col-lg-12">
            <?php

            ?>

            <?php // Html::activeInput('text', $model, 'ip', ['class' => 'form-control','id' => 'ip_input', 'maxlength' => true, 'placeholder' => 'Enter ip for druid']) ?>
        </div>
        <!--        <div class="col-lg-1">-->
        <!--            --><?php //// Html::submitButton('Get ip list', ['class' => 'btn btn-success', 'id' => 'getIpList']) ?>
        <!--        </div>-->
        <!--        <div class="col-lg-1">-->
        <!--            --><?php //// Html::submitButton('Use this ip', ['class' => 'btn btn-success', 'id' => 'useIp']) ?>
        <!--        </div>-->

        <!--        TODO iplist from coordinator-->
    </div>

    <br/>
    <!--   End of Ip selection section-->

    <!--    Druid query builder section-->
    <div class="well tabs">
        <div class="row">
            <div class="col-md-12">
                <ul class="nav nav-tabs dashboard-tabs">
                    <li class="active"><a href="#timeseries" data-toggle="tab">Timeseries</a></li>
                    <li><a href="#topN" data-toggle="tab">TopN</a></li>
                    <li><a href="#groupBy" data-toggle="tab">GroupBy</a></li>
                    <li><a href="#search" data-toggle="tab">Search</a></li>
                    <li><a href="#select" data-toggle="tab">Select</a></li>
                    <li><a href="#time-boundary" data-toggle="tab">Time Boundary</a></li>
                    <li><a href="#segment-metadata" data-toggle="tab">Segment Metadata</a></li>
                    <li><a href="#datasource-metadata" data-toggle="tab">DataSource Metadata</a></li>
                </ul>
            </div>
        </div>


        <div class="tab-content">
            <div class="tab-pane fade in active" id="timeseries">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'timeseries-form',
                    'action' => false
                ]);
                ?>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?=
                        $form->field($model, 'ip')
                            ->textInput(['id' => 'ip_input', 'maxlength' => true, 'placeholder' => 'Enter ip for druid']);
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'datasource')->dropDownList($model->getDatasourceList(), ['class' => 'form-control dimension-control', 'dimension' => 'dataSource', 'id' => false]); ?>
                    </div>
                </div>

                <div class="row intervals">
                    <div class="col-md-6">
                        <label class="control-label">Start date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->start_date]); ?>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">End date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->end_date]); ?>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'granularity')->dropDownList($model->getGranularityList(), ['class' => 'form-control dimension-control', 'dimension' => 'granularity']); ?>
                    </div>
                </div>
                <hr/>
                <?= $form->field($model, 'aggregations')->checkbox(['class' => 'enable-aggregation'])->label("Aggregations settings:"); ?>

                <div class="aggregation-settings hide">
                    <div class="row">
                        <div class="col-lg-5">
                            <?php echo $form->field($model, 'aggregations')->textArea(['rows' => '10', 'id' => false, 'class' => 'form-control aggregations-textbox', 'value' => \app\models\DruidBuilder::getAggregation()])->label(false); ?>
                        </div>
                    </div>


                    <?php echo Html::a('Add aggregation', [''], ['class' => 'btn btn-success add-aggregation']); ?>
                    <?php echo Html::a('Add to query', ['site/add-aggregation'], ['class' => 'btn btn-success save-aggregation', 'dimension' => 'aggregations']); ?>
                </div>

                <hr/>
                <?= $form->field($model, 'context')->checkbox(['class' => 'enable-option', 'childClass' => 'context-settings', 'href' => 'site/context-switch'])->label("Use context settings:"); ?>

                <div class="context-settings hide">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextTimeoutList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'timeout'])->label("Timeout: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextPriorityList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'priority'])->label("Priority: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextChunkPeriodList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'chunkPeriod'])->label("chunkPeriod: "); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'populateCache'])->label("populateCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'useCache'])->label("useCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'bySegment'])->label("bySegment: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'finalize'])->label("finalize: "); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Query:</strong></p>
                        <?php echo $form->field($model, 'query')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control query-textbox', 'value' => $model->getTimeseriesTemplate()])->label(false); ?>
                        <?= Html::submitButton('Send', [ 'class' => 'btn btn-success sendQuery']) ?>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Response:</strong></p>
                        <?php echo $form->field($model, 'response')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control response-textbox', 'onclick' => "this.focus();this.select()"])->label(false); ?>
                        <?= Html::Button('Clear', [ 'class' => 'btn btn-success clearResponse']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>


            <div class="tab-pane fade" id="topN">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'topN-form',
                    'action' => false
                ]);
                ?>
                <br/>
                <?=
                $form->field($model, 'ip')->textInput(['id' => 'ip_input', 'maxlength' => true, 'placeholder' => 'Enter ip for druid']);
                ?>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'datasource')->dropDownList($model->getDatasourceList(), ['class' => 'form-control dimension-control', 'dimension' => 'dataSource']); ?>
                    </div>
                </div>

                <div class="row intervals">
                    <div class="col-md-6">
                        <label class="control-label">Start date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->start_date]); ?>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">End date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->end_date]); ?>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'granularity')->dropDownList($model->getGranularityList(), ['class' => 'form-control dimension-control', 'dimension' => 'granularity']); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'dimension')->dropDownList($model->getDimensionList(), ['class' => 'form-control dimension-control', 'dimension' => 'dimension']); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'metric')->textInput(['class' => 'form-control dimension-control', 'dimension' => 'metric', 'value' => 'total_count', 'maxlength' => true, 'placeholder' => 'Enter metric']); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'threshold')->dropDownList($model->getThresholdList(), ['class' => 'form-control dimension-control', 'dimension' => 'threshold']); ?>
                    </div>
                </div>
                <hr/>
                <?= $form->field($model, 'aggregations')->checkbox(['class' => 'enable-aggregation'])->label("Aggregations settings:"); ?>

                <div class="aggregation-settings hide">
                    <div class="row">
                        <div class="col-lg-5">
                            <?php echo $form->field($model, 'aggregations')->textArea(['rows' => '10', 'id' => false, 'class' => 'form-control aggregations-textbox', 'value' => \app\models\DruidBuilder::getAggregation()])->label(false); ?>
                        </div>
                    </div>


                    <?php echo Html::a('Add aggregation', [''], ['class' => 'btn btn-success add-aggregation']); ?>
                    <?php echo Html::a('Add to query', ['site/add-aggregation'], ['class' => 'btn btn-success save-aggregation', 'dimension' => 'aggregations']); ?>
                </div>


                <hr/>
                <?= $form->field($model, 'context')->checkbox(['class' => 'enable-option', 'childClass' => 'context-settings', 'href' => 'site/context-switch'])->label("Use context settings:"); ?>

                <div class="context-settings hide">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextTimeoutList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'timeout'])->label("Timeout: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextPriorityList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'priority'])->label("Priority: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextChunkPeriodList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'chunkPeriod'])->label("chunkPeriod: "); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'populateCache'])->label("populateCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'useCache'])->label("useCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'bySegment'])->label("bySegment: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'finalize'])->label("finalize: "); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Query:</strong></p>
                        <?php echo $form->field($model, 'query')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control query-textbox', 'value' => $model->getTopnTemplate()])->label(false); ?>
                        <?= Html::submitButton('Send', [ 'class' => 'btn btn-success sendQuery']) ?>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Response:</strong></p>
                        <?php echo $form->field($model, 'response')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control response-textbox', 'onclick' => "this.focus();this.select()"])->label(false); ?>
                        <?= Html::Button('Clear', [ 'class' => 'btn btn-success clearResponse']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>
            <div class="tab-pane fade" id="groupBy">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'groupBy-form',
                    'action' => false
                ]);
                ?>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?=
                        $form->field($model, 'ip')
                            ->textInput(['id' => 'ip_input', 'maxlength' => true, 'placeholder' => 'Enter ip for druid']);
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'datasource')->dropDownList($model->getDatasourceList(), ['class' => 'form-control dimension-control', 'dimension' => 'dataSource', 'id' => false]); ?>
                    </div>
                </div>

                <div class="row intervals">
                    <div class="col-md-6">
                        <label class="control-label">Start date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->start_date]); ?>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">End date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->end_date]); ?>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'granularity')->dropDownList($model->getGranularityList(), ['class' => 'form-control dimension-control', 'dimension' => 'granularity']); ?>
                    </div>
                </div>

                <!--Dimensions settings -->
                <p class="text-left"><strong>Dimensions:</strong></p>
                <div class="row">
                    <div class="col-lg-4">
                        <?= $form->field($model, 'dimensions')->dropDownList($model->getDimensionList(), ['class' => 'form-control dimensions', 'dimension' => 'dimensions', 'dimensionType' => 'array'])->label(false); ?>
                    </div>
                    <div class="col-lg-2">
                        <?php echo Html::a('Add dimension', [''], ['class' => 'btn btn-success add-dimension']); ?>

                    </div>
                    <div class="col-lg-6"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <?php echo $form->field($model, 'dimensions')->textArea(['rows' => '', 'id' => false, 'class' => 'form-control dimensions-textbox', 'value' => ''])->label(false); ?>
                    </div>
                </div>
                <?php echo Html::a('Add to query', ['site/save-dimensions'], ['class' => 'btn btn-success save-dimension', 'dimension' => 'dimensions', 'dimensionType' => 'array', 'textboxClass' => 'dimensions-textbox']); ?>
                <!-- End dimensions settings -->
                <hr/>
                <?= $form->field($model, 'aggregations')->checkbox(['class' => 'enable-aggregation'])->label("Aggregations settings:"); ?>

                <div class="aggregation-settings hide">
                    <div class="row">
                        <div class="col-lg-5">
                            <?php echo $form->field($model, 'aggregations')->textArea(['rows' => '10', 'id' => false, 'class' => 'form-control aggregations-textbox', 'value' => \app\models\DruidBuilder::getAggregation()])->label(false); ?>
                        </div>
                    </div>


                    <?php echo Html::a('Add aggregation', [''], ['class' => 'btn btn-success add-aggregation']); ?>
                    <?php echo Html::a('Add to query', ['site/add-aggregation'], ['class' => 'btn btn-success save-aggregation', 'dimension' => 'aggregations']); ?>
                </div>


                <hr/>
                <?= $form->field($model, 'context')->checkbox(['class' => 'enable-option', 'childClass' => 'context-settings', 'href' => 'site/context-switch'])->label("Use context settings:"); ?>

                <div class="context-settings hide">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextTimeoutList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'timeout'])->label("Timeout: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextPriorityList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'priority'])->label("Priority: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextChunkPeriodList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'chunkPeriod'])->label("chunkPeriod: "); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'populateCache'])->label("populateCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'useCache'])->label("useCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'bySegment'])->label("bySegment: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'finalize'])->label("finalize: "); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Query:</strong></p>
                        <?php echo $form->field($model, 'query')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control query-textbox', 'value' => $model->getGroupByTemplate()])->label(false); ?>
                        <?= Html::submitButton('Send', ['class' => 'btn btn-success sendQuery']) ?>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Response:</strong></p>
                        <?php echo $form->field($model, 'response')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control response-textbox', 'onclick' => "this.focus();this.select()"])->label(false); ?>
                        <?= Html::Button('Clear', [ 'class' => 'btn btn-success clearResponse']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>
            <div class="tab-pane fade" id="search">
                <?php
                $form = ActiveForm::begin([
                    'id' => 'search-form',
                    'action' => false
                ]);
                ?>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?=
                        $form->field($model, 'ip')
                            ->textInput(['id' => 'ip_input', 'maxlength' => true, 'placeholder' => 'Enter ip for druid']);
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'datasource')->dropDownList($model->getDatasourceList(), ['class' => 'form-control dimension-control', 'dimension' => 'dataSource', 'id' => false]); ?>
                    </div>
                </div>

                <div class="row intervals">
                    <div class="col-md-6">
                        <label class="control-label">Start date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->start_date]); ?>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">End date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->end_date]); ?>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'granularity')->dropDownList($model->getGranularityList(), ['class' => 'form-control dimension-control', 'dimension' => 'granularity']); ?>
                    </div>
                </div>
                <hr />
                <p class="text-left"><strong>Search settings:</strong></p>
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'search_setting')->textInput(['class' => 'form-control dimension-control', 'dimension' => 'query', 'second_dimension' => 'value', 'maxlength' => true, 'placeholder' => 'Enter search value'])->label("Search value:"); ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'search_setting')->dropDownList($model->getSearchTypeList(), ['class' => 'form-control dimension-control', 'dimension' => 'query', 'second_dimension' => 'type', 'dimensionType' => 'array'])->label('Search type:'); ?>
                    </div>
                </div>

                <hr/>
                <?= $form->field($model, 'dimensions')->checkbox(['class' => 'enable-option', 'childClass' => 'search-dimensions-settings', 'href' => 'site/search-dimension-switch'])->label("Use search dimension settings (default - all):"); ?>

                <div class="search-dimensions-settings hide">
                    <!--Dimensions settings -->
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'dimensions')->dropDownList($model->getDimensionList(), ['class' => 'form-control dimensions', 'dimension' => 'dimensions', 'dimensionType' => 'array'])->label(false); ?>
                        </div>
                        <div class="col-lg-2">
                            <?php echo Html::a('Add dimension', [''], ['class' => 'btn btn-success add-dimension']); ?>
                        </div>
                        <div class="col-lg-6"></div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <?php echo $form->field($model, 'dimensions')->textArea(['rows' => '', 'id' => false, 'class' => 'form-control dimensions-textbox', 'value' => ''])->label(false); ?>
                        </div>
                    </div>
                    <?php echo Html::a('Add to query', ['site/save-dimensions'], ['class' => 'btn btn-success save-dimension', 'dimension' => 'searchDimensions', 'dimensionType' => 'array', 'textboxClass' => 'dimensions-textbox']); ?>
                    <!-- End dimensions settings -->

                </div>

                <hr/>
                <?= $form->field($model, 'sort')->checkbox(['class' => 'enable-option', 'childClass' => 'search-sort-settings', 'href' => 'site/search-sort-switch'])->label("Use sort settings:"); ?>

                <div class="search-sort-settings hide">
                    <div class="row">
                        <div class="col-lg-12">
                            <?= $form->field($model, 'sort')->dropDownList($model->getSortList(), ['class' => 'form-control dimension-control', 'dimension' => 'sort', 'second_dimension' => 'type'])->label(false); ?>
                        </div>
                    </div>
                </div>



                <hr/>
                <?= $form->field($model, 'context')->checkbox(['class' => 'enable-option', 'childClass' => 'context-settings', 'href' => 'site/context-switch'])->label("Use context settings:"); ?>

                <div class="context-settings hide">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextTimeoutList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'timeout'])->label("Timeout: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextPriorityList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'priority'])->label("Priority: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextChunkPeriodList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'chunkPeriod'])->label("chunkPeriod: "); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'populateCache'])->label("populateCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'useCache'])->label("useCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'bySegment'])->label("bySegment: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'finalize'])->label("finalize: "); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Query:</strong></p>
                        <?php echo $form->field($model, 'query')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control query-textbox', 'value' => $model->getSearchTemplate()])->label(false); ?>
                        <?= Html::submitButton('Send', [ 'class' => 'btn btn-success sendQuery']) ?>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Response:</strong></p>
                        <?php echo $form->field($model, 'response')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control response-textbox', 'onclick' => "this.focus();this.select()"])->label(false); ?>
                        <?= Html::Button('Clear', [ 'class' => 'btn btn-success clearResponse']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>
            <div class="tab-pane fade" id="select">

                <?php
                $form = ActiveForm::begin([
                    'id' => 'select-form',
                    'action' => false
                ]);
                ?>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?=
                        $form->field($model, 'ip')
                            ->textInput(['id' => 'ip_input', 'maxlength' => true, 'placeholder' => 'Enter ip for druid']);
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'datasource')->dropDownList($model->getDatasourceList(), ['class' => 'form-control dimension-control', 'dimension' => 'dataSource', 'id' => false]); ?>
                    </div>
                </div>

                <div class="row intervals">
                    <div class="col-md-6">
                        <label class="control-label">Start date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->start_date]); ?>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">End date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->end_date]); ?>
                    </div>
                </div>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'granularity')->dropDownList($model->getGranularityList(), ['class' => 'form-control dimension-control', 'dimension' => 'granularity']); ?>
                    </div>
                </div>

                <!--Dimensions settings -->
                <p class="text-left"><strong>Dimensions:</strong></p>
                <div class="row">
                    <div class="col-lg-4">
                        <?= $form->field($model, 'dimensions')->dropDownList($model->getDimensionList(), ['class' => 'form-control dimensions', 'dimension' => 'dimensions', 'dimensionType' => 'array'])->label(false); ?>
                    </div>
                    <div class="col-lg-2">
                        <?php echo Html::a('Add dimension', [''], ['class' => 'btn btn-success add-dimension']); ?>

                    </div>
                    <div class="col-lg-6"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <?php echo $form->field($model, 'dimensions')->textArea(['rows' => '', 'id' => false, 'class' => 'form-control dimensions-textbox', 'value' => ''])->label(false); ?>
                    </div>
                </div>
                <?php echo Html::a('Add to query', ['site/save-dimensions'], ['class' => 'btn btn-success save-dimension', 'dimension' => 'dimensions', 'dimensionType' => 'array', 'textboxClass' => 'dimensions-textbox']); ?>
                <!-- End dimensions settings -->
                <br /><br />
                <!-- Metrics settings -->
                <p class="text-left"><strong>Metrics:</strong></p>
                <div class="row">
                    <div class="col-lg-4">
                        <?= $form->field($model, 'metric')->textInput(['class' => 'form-control metric', 'value' => '', 'maxlength' => true, 'placeholder' => 'Enter metric'])->label(false); ?>
                    </div>
                    <div class="col-lg-2">
                        <?php echo Html::a('Add metric', [''], ['class' => 'btn btn-success add-metric']); ?>

                    </div>
                    <div class="col-lg-6"></div>
                </div>
                <div class="row">
                    <div class="col-lg-6">
                        <?php echo $form->field($model, 'metrics')->textArea(['rows' => '', 'id' => false, 'class' => 'form-control metrics-textbox', 'value' => ''])->label(false); ?>
                    </div>
                </div>
                <?php echo Html::a('Add to query', ['site/save-metrics'], ['class' => 'btn btn-success save-dimension', 'dimension' => 'metrics', 'dimensionType' => 'array', 'textboxClass' => 'metrics-textbox']); ?>
                <!-- End metrics settings -->
                

                <hr/>
                <?= $form->field($model, 'aggregations')->checkbox(['class' => 'enable-aggregation'])->label("Aggregations settings:"); ?>

                <div class="aggregation-settings hide">
                    <div class="row">
                        <div class="col-lg-5">
                            <?php echo $form->field($model, 'aggregations')->textArea(['rows' => '10', 'id' => false, 'class' => 'form-control aggregations-textbox', 'value' => \app\models\DruidBuilder::getAggregation()])->label(false); ?>
                        </div>
                    </div>


                    <?php echo Html::a('Add aggregation', [''], ['class' => 'btn btn-success add-aggregation']); ?>
                    <?php echo Html::a('Add to query', ['site/add-aggregation'], ['class' => 'btn btn-success save-aggregation', 'dimension' => 'aggregations']); ?>
                </div>


                <hr/>
                <?= $form->field($model, 'context')->checkbox(['class' => 'enable-option', 'childClass' => 'context-settings', 'href' => 'site/context-switch'])->label("Use context settings:"); ?>

                <div class="context-settings hide">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextTimeoutList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'timeout'])->label("Timeout: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextPriorityList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'priority'])->label("Priority: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextChunkPeriodList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'chunkPeriod'])->label("chunkPeriod: "); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'populateCache'])->label("populateCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'useCache'])->label("useCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'bySegment'])->label("bySegment: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'finalize'])->label("finalize: "); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Query:</strong></p>
                        <?php echo $form->field($model, 'query')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control query-textbox', 'value' => $model->getSelectTemplate()])->label(false); ?>
                        <?= Html::submitButton('Send', ['class' => 'btn btn-success sendQuery']) ?>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Response:</strong></p>
                        <?php echo $form->field($model, 'response')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control response-textbox', 'onclick' => "this.focus();this.select()"])->label(false); ?>
                        <?= Html::Button('Clear', [ 'class' => 'btn btn-success clearResponse']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>

            </div>
            <div class="tab-pane fade" id="time-boundary">

                <?php
                $form = ActiveForm::begin([
                    'id' => 'time-boundary-form',
                    'action' => false
                ]);
                ?>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?=
                        $form->field($model, 'ip')
                            ->textInput(['id' => 'ip_input', 'maxlength' => true, 'placeholder' => 'Enter ip for druid']);
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'datasource')->dropDownList($model->getDatasourceList(), ['class' => 'form-control dimension-control', 'dimension' => 'dataSource', 'id' => false]); ?>
                    </div>
                </div>
                <hr/>
                <?= $form->field($model, 'context')->checkbox(['class' => 'enable-option', 'childClass' => 'context-settings', 'href' => 'site/context-switch'])->label("Use context settings:"); ?>

                <div class="context-settings hide">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextTimeoutList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'timeout'])->label("Timeout: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextPriorityList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'priority'])->label("Priority: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextChunkPeriodList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'chunkPeriod'])->label("chunkPeriod: "); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'populateCache'])->label("populateCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'useCache'])->label("useCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'bySegment'])->label("bySegment: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'finalize'])->label("finalize: "); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Query:</strong></p>
                        <?php echo $form->field($model, 'query')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control query-textbox', 'value' => $model->getTimeBoundaryTemplate()])->label(false); ?>
                        <?= Html::submitButton('Send', ['class' => 'btn btn-success sendQuery']) ?>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Response:</strong></p>
                        <?php echo $form->field($model, 'response')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control response-textbox', 'onclick' => "this.focus();this.select()"])->label(false); ?>
                        <?= Html::Button('Clear', [ 'class' => 'btn btn-success clearResponse']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>

            </div>
            <div class="tab-pane fade" id="segment-metadata">

                <?php
                $form = ActiveForm::begin([
                    'id' => 'segment-metadata-form',
                    'action' => false
                ]);
                ?>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?=
                        $form->field($model, 'ip')
                            ->textInput(['id' => 'ip_input', 'maxlength' => true, 'placeholder' => 'Enter ip for druid']);
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'datasource')->dropDownList($model->getDatasourceList(), ['class' => 'form-control dimension-control', 'dimension' => 'dataSource', 'id' => false]); ?>
                    </div>
                </div>

                <div class="row intervals">
                    <div class="col-md-6">
                        <label class="control-label">Start date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->start_date]); ?>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">End date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->end_date]); ?>
                    </div>
                </div>
                <hr/>
                <?= $form->field($model, 'context')->checkbox(['class' => 'enable-option', 'childClass' => 'context-settings', 'href' => 'site/context-switch'])->label("Use context settings:"); ?>

                <div class="context-settings hide">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextTimeoutList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'timeout'])->label("Timeout: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextPriorityList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'priority'])->label("Priority: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextChunkPeriodList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'chunkPeriod'])->label("chunkPeriod: "); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'populateCache'])->label("populateCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'useCache'])->label("useCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'bySegment'])->label("bySegment: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'finalize'])->label("finalize: "); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Query:</strong></p>
                        <?php echo $form->field($model, 'query')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control query-textbox', 'value' => $model->getSegmentMetadataTemplate()])->label(false); ?>
                        <?= Html::submitButton('Send', ['class' => 'btn btn-success sendQuery']) ?>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Response:</strong></p>
                        <?php echo $form->field($model, 'response')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control response-textbox', 'onclick' => "this.focus();this.select()"])->label(false); ?>
                        <?= Html::Button('Clear', [ 'class' => 'btn btn-success clearResponse']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="tab-pane fade" id="datasource-metadata">

                <?php
                $form = ActiveForm::begin([
                    'id' => 'datasource-metadata-form',
                    'action' => false
                ]);
                ?>
                <br/>

                <div class="row">
                    <div class="col-lg-12">
                        <?=
                        $form->field($model, 'ip')
                            ->textInput(['id' => 'ip_input', 'maxlength' => true, 'placeholder' => 'Enter ip for druid']);
                        ?>

                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <?= $form->field($model, 'datasource')->dropDownList($model->getDatasourceList(), ['class' => 'form-control dimension-control', 'dimension' => 'dataSource', 'id' => false]); ?>
                    </div>
                </div>

                <div class="row intervals">
                    <div class="col-md-6">
                        <label class="control-label">Start date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->start_date]); ?>
                    </div>

                    <div class="col-md-6">
                        <label class="control-label">End date:</label>
                        <?= $this->render('_datepicker', ['model' => $model, 'date' => $model->end_date]); ?>
                    </div>
                </div>
                <hr/>
                <?= $form->field($model, 'context')->checkbox(['class' => 'enable-option', 'childClass' => 'context-settings', 'href' => 'site/context-switch'])->label("Use context settings:"); ?>

                <div class="context-settings hide">
                    <div class="row">
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextTimeoutList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'timeout'])->label("Timeout: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextPriorityList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'priority'])->label("Priority: "); ?>
                        </div>
                        <div class="col-lg-4">
                            <?= $form->field($model, 'context')->dropDownList($model->getContextChunkPeriodList(), ['class' => 'form-control dimension-control', 'dimension' => 'context', 'second_dimension' => 'chunkPeriod'])->label("chunkPeriod: "); ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'populateCache'])->label("populateCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'useCache'])->label("useCache: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'bySegment'])->label("bySegment: "); ?>
                        </div>
                        <div class="col-lg-3">
                            <?= $form->field($model, 'context')->checkbox(['class' => 'dimension-control', 'dimension' => 'context', 'second_dimension' => 'finalize'])->label("finalize: "); ?>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Query:</strong></p>
                        <?php echo $form->field($model, 'query')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control query-textbox', 'value' => $model->getDataSourceMetadataTemplate()])->label(false); ?>
                        <?= Html::submitButton('Send', ['class' => 'btn btn-success sendQuery']) ?>
                    </div>
                    <div class="col-lg-6">
                        <p class="text-left"><strong>Response:</strong></p>
                        <?php echo $form->field($model, 'response')->textArea(['rows' => '20', 'id' => false, 'class' => 'form-control response-textbox', 'onclick' => "this.focus();this.select()"])->label(false); ?>
                        <?= Html::Button('Clear', [ 'class' => 'btn btn-success clearResponse']) ?>
                    </div>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    <!-- End of Druid query builder section-->
</div>

<?php
$this->registerJsFile('js/site.js?3', ['depends' => yii\web\JqueryAsset::className()]);
?>
<?php
//$script = <<< JS
//
//
//JS;
//
//$this->registerJs($script);

?>
