<?php
use \kartik\datetime\DateTimePicker;

echo DateTimePicker::widget([
    'name' => 'start_date',
    'type' => DateTimePicker::TYPE_COMPONENT_PREPEND,
    'value' => $date,
    'options' => ['class' => 'interval', 'dimension' => 'intervals', 'dimensionType' => 'array'],
    'pluginOptions' => [
        'autoclose' => true,
        //'format' => 'dd-M-yyyy hh:ii'
        'format' => 'yyyy-mm-ddThh:ii:ss'
    ],
    'removeButton' => false
]);
