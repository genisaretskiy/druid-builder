<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\helpers\Json;

/**
 * DruidBuilder is the model behind the druid-builder form.
 */
class DruidBuilder extends Model
{
    public $ip;
    public $port;

    public $datasource;
    public $granularity;

    public $start_date;
    public $end_date;

    public $context;

    public $aggregations;

    public $query;

    public $response;

    //TopN query
    public $dimension;
    public $metric;
    public $threshold;

    //GroupBy query
    public $dimensions;

    //Search query
    public $sort;
    public $query_setting;
    public $search_setting;

    public $params;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['ip', 'port', 'params'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'context' => '',
            'ip' => 'Druid ip:port :',
            'aggregations' => '',
            'search_setting' => '',
            'dimensions' => '',
            'sort' => '',
            'datasource' => 'Datasource:',
//            'start_date' => 'Start date:',
//            'end_date' => 'End date:',
            'granularity' => 'Granularity:'
        ];
    }

    public function getDatasourceList()
    {
        return [
            '0' => 'datasource-topic-click',
            '1' => 'datasource-topic-lead',
            '2' => 'datasource-topic-error',
            '3' => 'datasource-topic-landing',
            '4' => 'datasource-topic-click-new',
            '5' => 'datasource-topic-landing-new',
            '6' => 'datasource-topic-lead-new',
            '7' => 'datasource-topic-error-new'
        ];

    }

    public function getGranularityList()
    {
        return [
            '0' => 'all',
            '1' => 'none',
            '2' => 'minute',
            '3' => 'fifteen_minute',
            '4' => 'thirty_minute',
            '5' => 'hour',
            '6' => 'day'
        ];

    }

    public function getContextTimeoutList()
    {
        return [
            '0' => '10000',
            '1' => '60000',
            '2' => '300000'
        ];

    }

    public function getContextPriorityList()
    {
        return [
            '0' => '1',
            '1' => '2',
            '2' => '3'
        ];

    }

    public function getContextChunkPeriodList()
    {
        return [
            '0' => 'P1D',
            '1' => 'P1W',
            '2' => 'P1M'
        ];

    }



    public function getDimensionList()
    {
        return [
            '1' => 'country',
            '2' => 'region',
            '3' => 'city',
            '4' => 'orientation_change',
            '5' => 'device_type',
            '6' => 'traffic_source_id',
            '7' => 'platform',
            '8' => 'device_name',
            '9' => 'operating_system',
            '10' => 'browser_version',
            '11' => 'pointing_method',
            '12' => 'campaign_id',
            '13' => 'connection_type',
            '14' => 'browser_name',
            '15' => 'device_brand'
        ];

    }

    public function getThresholdList()
    {
        return [
            '0' => '5',
            '1' => '10',
            '2' => '30',
            '3' => '100',
            '4' => '300',
            '5' => '1000'
        ];

    }
    public function getSearchTypeList()
    {
        return [
            '0' => 'insensitive_contains',
            '1' => 'fragment'
        ];

    }


    public function getSortList()
    {
        return [
            '0' => 'lexicographic',
            '1' => 'strlen'
        ];

    }

    public static function getAggregation()
    {
        return Json::encode(

            [
                "type" => "count",
                "name" => "total_count"
            ]
        );
    }

    public static function getContextTemplate()
    {
        return [
            "timeout" => 10000,
            "priority" => 1,
            "useCache" => "false",
            "populateCache" => "false",
            "bySegment" => "false",
            "finalize" => "false",
            "chunkPeriod" => "P1D"
        ];
    }

    public static function getSearchSortTemplate() {
        return [
            "type" => "lexicographic"
        ];
    }

    /**
     * @return string
     */
    public function getTimeseriesTemplate()
    {
        return Json::encode([
            "queryType" => "timeseries",
            "dataSource" => $this->getDatasourceList()[0],
            "intervals" => ["$this->start_date$this->end_date"],
            "granularity" => "all",
            "aggregations" => [[
                "type" => "count",
                "name" => "total_count"
            ]]
        ], JSON_PRETTY_PRINT);
    }

    public function getTopnTemplate()
    {
        return Json::encode([
            "queryType" => "topN",
            "dataSource" => $this->getDatasourceList()[0],
            "intervals" => ["$this->start_date$this->end_date"],
            "granularity" => "all",
            "dimension" => 'custom',
            "metric" => "total_count",
            "aggregations" => [[
                "type" => "count",
                "name" => "total_count"
            ]],
            "threshold" => "5"
        ], JSON_PRETTY_PRINT);
    }

    public function getGroupByTemplate()
    {
        return Json::encode([
            "queryType" => "groupBy",
            "dataSource" => $this->getDatasourceList()[0],
            "intervals" => ["$this->start_date$this->end_date"],
            "granularity" => "all",
            "dimensions" => [$this->getDimensionList()[1]],
            "aggregations" => [[
                "type" => "count",
                "name" => "total_count"
            ]]
        ], JSON_PRETTY_PRINT);
    }

    public function getSearchTemplate()
    {
        return Json::encode([
            "queryType" => "search",
            "dataSource" => $this->getDatasourceList()[0],
            "intervals" => ["$this->start_date$this->end_date"],
            "granularity" => "all",
            "query" => [
                "type" => "insensitive_contains",
                "value" => "city"
            ],
        ], JSON_PRETTY_PRINT);
    }

    public function getSelectTemplate()
    {
        return Json::encode([
            "queryType" => "select",
            "dataSource" => $this->getDatasourceList()[0],
            "intervals" => ["$this->start_date$this->end_date"],
            "granularity" => "all",
            "dimensions" => [],
            "metrics" => [],
            "pagingSpec" => [
                "pagingIdentifiers" => [""=>""], "threshold" => 5]

        ], JSON_PRETTY_PRINT);
    }

    public function getTimeBoundaryTemplate()
    {
        return Json::encode([
            "queryType" => "timeBoundary",
            "dataSource" => $this->getDatasourceList()[0]
        ], JSON_PRETTY_PRINT);
    }

    public function getSegmentMetadataTemplate()
    {
        return Json::encode([
            "queryType" => "segmentMetadata",
            "dataSource" => $this->getDatasourceList()[0],
            "intervals" => ["$this->start_date$this->end_date"]
        ], JSON_PRETTY_PRINT);
    }

    public function getDataSourceMetadataTemplate()
    {
        return Json::encode([
            "queryType" => "dataSourceMetadata",
            "dataSource" => $this->getDatasourceList()[0]
        ], JSON_PRETTY_PRINT);
    }



    /**
     * @param string $url
     * @param $content
     * @return mixed
     */
    public static function sendQuery($url, $content)
    {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER,
            ["Content-type: application/json"]);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $content);

        $json_response = curl_exec($curl);

        $status = curl_getinfo($curl, CURLINFO_HTTP_CODE);

//        if ( $status != 201 ) {
//            die("Error: call to URL $url failed with status $status, response $json_response, curl_error " . curl_error($curl) . ", curl_errno " . curl_errno($curl));
//        }


        curl_close($curl);
        return Json::decode($json_response);
    }

//End of context settings
}
