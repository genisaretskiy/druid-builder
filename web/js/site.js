$(document).on('change', '.dimension-control', function () {
    var queryId = $(this).parents('[id]:eq(0)').attr('id');
    var query = $('#'+queryId).find('.query-textbox').val();
    var dimension = $(this).attr('dimension');
    var second_dimension = $(this).attr('second_dimension');
    var dimension_type = $(this).attr('dimensionType');

    if($(this).is('select')) {
        var value = $("option:selected", $(this)).text();
    } else if ($(this).is('[type=text]')) {
        var value = $($(this)).val();
    } else {
        var value = $($(this)).text();
    }

    if(value.length == 0) {
        if(this.checked) {
            value = true;
        } else {
            value = false;
        }
    }
    $.ajax({
        type: "GET",
        url: "site/query",
        data: {
            'value': value,
            'query': query,
            'dimension': dimension,
            'second_dimension': second_dimension,
            'dimension_type': dimension_type
        },
        dataType: "json",
        success: function (data) {
            $('#'+queryId).find('.query-textbox').val(JSON.stringify(data, null, 4));
        }
    });
})
$(document).on('change', '.intervals', function () {
    var intervals = '';

    $(this).find('.interval').each( function( index){
        if(index == 0) {
            intervals = $(this).val() + "/";
        } else {
            intervals += $(this).val();
        }
    });

    var queryId = $(this).parents('[id]:eq(0)').attr('id');
    var query = $('#'+queryId).find('.query-textbox').val();

    var dimension = $(this).find('.interval').attr('dimension');
    var dimension_type = $(this).find('.interval').attr('dimensionType');

    $.ajax({
        type: "GET",
        url: "site/query",
        data: {
            'value': intervals,
            'query': query,
            'dimension': dimension,
            'dimension_type': dimension_type
        },
        dataType: "json",
        success: function (data) {
            $('#'+queryId).find('.query-textbox').val(JSON.stringify(data, null, 4));
        }
    });

});

$(document).on('change', '.enable-option', function () {
    var formId = $(this).parents('[id]:eq(0)').attr('id');
    var childClass = $('#'+formId).find(this).attr('childClass');
    var href = $('#'+formId).find(this).attr('href');

    if ($(this).is(':checked')) {
        $('#'+formId).find('.'+childClass).removeClass('hide');

    } else {
        $('#'+formId).find('.'+childClass).addClass('hide');
    }
    var query = $('#'+formId).find('.query-textbox').val();
    $.ajax({
        type: "GET",
        url: href,
        data: {
            'state': $(this).is(':checked'),
            'query': query
        },
        dataType: "json",
        success: function (data) {
            $('#'+formId).find('.query-textbox').val(JSON.stringify(data, null, 4));
        }
    });
}).trigger('change');
$(document).on('change', '.enable-aggregation', function () {
    var formId = $(this).parents('[id]:eq(0)').attr('id');

    if ($(this).is(':checked')) {
        $('#'+formId).find('.aggregation-settings').removeClass('hide');
    } else {
        $('#'+formId).find('.aggregation-settings').addClass('hide');
    }
}).trigger('change');

$(document).on('click', '.add-aggregation', function () {
    var formId = $(this).parents('[id]:eq(0)').attr('id');
    var aggregetion = $('#'+formId).find('.aggregations-textbox').val();

    $.ajax({
        type: "GET",
        url: "site/add-aggregation",
        data: {
            'aggregetion': aggregetion
        },
        success: function (data) {

            $('#'+formId).find('.aggregations-textbox').val(data);

        }
    });
    return false;
})
$(document).on('click', '.save-aggregation', function () {

    var queryId = $(this).parents('[id]:eq(0)').attr('id');
    var query = $('#'+queryId).find('.query-textbox').val();
    var dimension = $(this).attr('dimension');
    var value = '[' + $('#'+queryId).find('.aggregations-textbox').val() + ']';
    $.ajax({
        type: "GET",
        url: "site/query",
        data: {
            'value': value,
            'query': query,
            'dimension': dimension
        },
        dataType: "json",
        success: function (data) {
            $('#'+queryId).find('.query-textbox').val(JSON.stringify(data, null, 4));
        }
    });
    return false;
})

$(document).on('click', '.add-dimension', function () {
    var formId = $(this).parents('[id]:eq(0)').attr('id');
    var value = $('#'+formId).find('.dimensions option:selected').text();
    var old_value = $('#'+formId).find('.dimensions-textbox').val();

    if(old_value.length > 1) {
        $('#'+formId).find('.dimensions-textbox').val(old_value+", "+value);
    } else {
        $('#'+formId).find('.dimensions-textbox').val(value);
    }

    return false;
})
$(document).on('click', '.add-metric', function () {
    var formId = $(this).parents('[id]:eq(0)').attr('id');
    var value = $('#'+formId).find('.metric').val();

    var old_value = $('#'+formId).find('.metrics-textbox').val();

    if(old_value.length > 1) {
        $('#'+formId).find('.metrics-textbox').val(old_value+", "+value);
    } else {
        $('#'+formId).find('.metrics-textbox').val(value);
    }
    $('#'+formId).find('.metric').val('');
    return false;
})
$(document).on('click', '.save-dimension', function () {
    var queryId = $(this).parents('[id]:eq(0)').attr('id');
    var query = $('#'+queryId).find('.query-textbox').val();
    var dimension = $(this).attr('dimension');

    var textbox_class = $(this).attr('textboxClass');

    var value = $('#'+queryId).find('.'+textbox_class).val();
    var dimension_type = $(this).attr('dimensionType');
    $.ajax({
        type: "GET",
        url: "site/query",
        data: {
            'value': value,
            'query': query,
            'dimension': dimension,
            'dimension_type': dimension_type
        },
        dataType: "json",
        success: function (data) {
            $('#'+queryId).find('.query-textbox').val(JSON.stringify(data, null, 4));
        }
    });
    return false;
})


$(document).on('click', '.sendQuery', function () {
    var formId = $(this).parents('[id]:eq(0)').attr('id');
    var url = $('#ip_input').val();

    $('#'+formId).find('.sendQuery').prop("disabled",true);
    $.ajax({
        type: "POST",
        url: "site/send",
        data : $("#"+formId).serialize(),

        success: function (data) {
            $('#'+formId).find('.response-textbox').html(data);
            $('#'+formId).find('.sendQuery').prop("disabled", false);
        }
    });
    return false;
})

$(document).on('click', '.clearResponse', function () {
    var formId = $(this).parents('[id]:eq(0)').attr('id');
    $('#'+formId).find('.response-textbox').html('');
})




function syntaxHighlight(json) {
    if (typeof json != 'string') {
        json = JSON.stringify(json, undefined, 2);
    }
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}