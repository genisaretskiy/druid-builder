<?php

namespace app\controllers;

use app\models\DruidBuilder;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\helpers\Json;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
//                    'add-aggregation' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $model = new DruidBuilder();
        $model->ip = "52.29.246.154:8082";
        $model->query = $model->getTimeseriesTemplate();
        date_default_timezone_set(date_default_timezone_get());
        $date = date('Y-m-d');
        $model->start_date = $date . 'T' . '00:00:00/';
        $model->end_date = $date . 'T' . '23:00:00';

        return $this->render('index', ['model' => $model]);



    }

    public function actionContextSwitch($state, $query)
    {
        if (Yii::$app->request->isAjax) {
            $query = Json::decode($query);
            if ($state === 'true') {
                $query['context'] = DruidBuilder::getContextTemplate();
            } else {
                unset($query['context']);
            }

            return Json::encode($query);
        }
    }

    public function actionAddAggregation($aggregetion)
    {
        if (Yii::$app->request->isAjax) {
            return $aggregetion . "," . DruidBuilder::getAggregation();
        }
    }

    public function actionSend()
    {
        if (Yii::$app->request->post('DruidBuilder')) {
//            $model->ip = "http://52.29.246.154:8082/druid/v2";
            $url = Yii::$app->request->post('DruidBuilder')['ip'].'/druid/v2';

            $query = Yii::$app->request->post('DruidBuilder')['query'];
            $start = microtime(true);
            $result['time'] = 0;
            $result['response'] = DruidBuilder::sendQuery($url, $query);
            $result['time'] = microtime(true) - $start;
            return Json::encode($result, JSON_PRETTY_PRINT);

        }

    }

    public function actionQuery($value, $query, $dimension, $second_dimension = null, $dimension_type = null)
    {
        if (Yii::$app->request->isAjax) {
            $query = Json::decode($query);

            if ($second_dimension) {
                if ($second_dimension == 'timeout' || $second_dimension == 'priority') {
                    $query[$dimension][$second_dimension] = intval($value);
                } else {
                    $query[$dimension][$second_dimension] = $value;
                }

            } else {
                if ($dimension == 'aggregations') {
                    $query[$dimension] = Json::decode($value);
                } else if ($dimension_type == 'array') {
                    $query[$dimension] = [$value];
                } else {
                    $query[$dimension] = $value;
                }
            }

            return Json::encode($query);
        }

    }

    public function actionSearchDimensionSwitch($state, $query)
    {
        if (Yii::$app->request->isAjax) {
            $query = Json::decode($query);
            if ($state === 'true') {
                $query['searchDimensions'] = [];
            } else {
                unset($query['searchDimensions']);
            }
            return Json::encode($query);
        }
    }

    public function actionSearchSortSwitch($state, $query)
    {
        if (Yii::$app->request->isAjax) {
            $query = Json::decode($query);
            if ($state === 'true') {
                $query['sort'] = DruidBuilder::getSearchSortTemplate();
            } else {
                unset($query['sort']);
            }
            return Json::encode($query);
        }
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

}
